﻿
appController.controller("GoodsInwardPreAdviceView", ["$scope", "$filter",
    function ($scope, $filter) {
        $scope.delete = function (idx) {

            var deleteItemID = $scope.stores[idx];

            alert("Pre-Advice is " + deleteItemID.preAdvice);


            //var el = document.getElementById('btnSearch');
            //angular.element(el).triggerHandler('click');

        };
        var init;
        return $scope.stores = [{
            preAdvice: "171989",
            YourPO: "1234",
            PODate: "16/07/2014",
            BookedDate: "19/07/2014",
            SupplierName: "SPECIALITY DRINKS LTD",
            ShipperName: "Deevee Transits",
            Status: "Pre-Advice Entry",
            Lines: "1",
            ID: "1"
        }, {
            preAdvice: "171990",
            YourPO: "34",
            PODate: "12/07/2014",
            BookedDate: "11/07/2014",
            SupplierName: "SPECIALITY DRINKS LTD",
            ShipperName: "Deevee Transits",
            Status: "Pre-Advice Entry",
            Lines: "4",
            ID: "2"
        }, {
            preAdvice: "171991",
            YourPO: "124",
            PODate: "10/07/2014",
            BookedDate: "14/07/2014",
            SupplierName: "SPECIALITY DRINKS LTD",
            ShipperName: "Deevee Transits",
            Status: "Pre-Advice Entry",
            Lines: "2",
            ID: "3"
        }, {
            preAdvice: "171992",
            YourPO: "124",
            PODate: "15/07/2014",
            BookedDate: "17/07/2014",
            SupplierName: "SPECIALITY DRINKS LTD",
            ShipperName: "Transits",
            Status: "Pre-Advice Entry",
            Lines: "3",
            ID: "4"
        }], $scope.searchKeywords = "", $scope.filteredStores = [], $scope.row = "", $scope.select = function (page) {
            var end, start;
            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end)
        }, $scope.onFilterChange = function () {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        }, $scope.onNumPerPageChange = function () {
            return $scope.select(1), $scope.currentPage = 1
        }, $scope.onOrderChange = function () {
            return $scope.select(1), $scope.currentPage = 1
        }, $scope.search = function () {
            return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange()
        }, $scope.order = function (rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0
        }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageStores = [], (init = function () {
            return $scope.search(), $scope.select($scope.currentPage)
        }


        )()

    }
]);