﻿function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));


}
function showToolTip(ControlID, ToolTipID) {
    $('#' + ControlID).hover(function () {
        $('#' + ToolTipID).show();
        $('#' + ToolTipID).
                 position({ at: 'right left', of: $(this), my: 'left' })
    });
    $('#' + ControlID).mouseleave(function () {
        $('#' + ToolTipID).hide();
    });
}
function loadDatePicker() {
    $('input[name="startDate"').datepicker({ formate: 'dd/mm/yy ' });
    $('input[name="endDate"').datepicker({ formate: 'dd/mm/yy ' });

}