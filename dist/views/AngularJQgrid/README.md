The functionality here is that I have a list of people, those people
have names, ages and statuses.  In line with what we might do in a real
app, the status is a code, and we want to show the decode.  Accordingly
we have a status codes list (which might in a real app come from the database),
and we have a filter to map the code to the decode.

What we want are two things.  We'd like to be able to edit the name in
an input box, and to edit the status in a dropdown.

There are a couple of defects we're specifically chasing - the problem with
focus being triggered, and the untidiness about single and double clicks
on the cells.  A single click seems to half select the item, and the editable
cell template doesn't go away again.  This may be related to the focus issue.

Comments on things I've learned on this plunk.

1. At the gridOptions level, there are both enableCellEditOnFocus and 
   enableCellEdit.  Don't enable both, you need to pick.  onFocus means
   single click, CellEdit means double click.  If you enable both then
   you get unexpected behaviour on the bits of your grid you didn't want
   to be editable

2. At the columnDefs level, you have the same options.  But this time you
   need to set both CellEdit and onFocus, and you need to set cellEdit to 
   false on any cells you don't want edited - this isn't the default

3. The documentation says that your editable cell template can be:
    <input ng-class="'colt' + col.index" ng-input="COL_FIELD" />
  actually it needs to be:
    <input ng-class="'colt' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" />

4. To trigger a save event when we lose focus, we've created an blur directive,
   the logic for which I found in stackoverflow: http://stackoverflow.com/questions/15647981/angularjs-and-ng-grid-auto-save-data-to-the-server-after-a-cell-was-changed

5. This also means changing each editable cell template to call ng-blur, which 
   you can see at the end of the editable cell template

6. We get two blur events when we leave the field (at least in Chrome), so we
   use a timer so that only one of them is processed.  Ugly, but it works

More information can be found in two places:
http://stackoverflow.com/questions/15647981/angularjs-and-ng-grid-auto-save-data-to-the-server-after-a-cell-was-changed/
http://technpol.wordpress.com/2013/12/06/editable-nggrid-with-both-dropdowns-and-selects/