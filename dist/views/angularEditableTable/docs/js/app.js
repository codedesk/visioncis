//var app = angular.module("app", ["xeditable", "ngMockE2E"]);



app.run(function($rootScope, $httpBackend, editableOptions, editableThemes) {
  $rootScope.debug = {};
 
  // -- mocks --

  //groups
  $httpBackend.whenGET('/groups').respond([
    {id: 1, text: 'user'},
    {id: 2, text: 'customer'},
    {id: 3, text: 'vip'},
    {id: 4, text: 'admin'}
  ]);

  //groups err
  $httpBackend.whenGET('/groups-err').respond(function(method, url, data) {
    return [500, 'server error', {}];
  });

  //check name
  $httpBackend.whenPOST(/\/checkName/).respond(function(method, url, data) {
    data = angular.fromJson(data);
    //if(data.value !== 'awesome') {
    //  return [200, {status: 'error', msg: 'Username should be `awesome`'}]; 
    //} else {
      return [200, {status: 'ok'}]; 
   // }
  });

  //update user (for single field)
  $httpBackend.whenPOST(/\/updateUser/).respond(function(method, url, data) {
    data = angular.fromJson(data);
    //if(data.name !== 'awesome') {
    //  return [500, 'Server-side error: username should be `awesome`!']; 
    //} else {
      return [200, 'ok']; 
   // }
  });

  //save user (for forms)
  $httpBackend.whenPOST(/\/saveUser/).respond(function(method, url, data) {
    data = angular.fromJson(data);
    if(data.name === 'error') {
      return [500, {field: 'name', msg: 'Server-side error for this username!'}]; 
    } else {
      return [200, {status: 'ok'}]; 
    }
  });

  //save column
  $httpBackend.whenPOST(/\/saveColumn/).respond(function(method, url, data) {
    data = angular.fromJson(data);
    if(data.column === 'name' && data.value !== 'awesome') {
      return [500, 'Username should be `awesome`']; 
    } else {
      return [200, {status: 'ok'}]; 
    }
  });

  $httpBackend.whenGET(/\.(html|css|js)$/).passThrough();

});
app.controller('EditableRowCtrl', function ($scope, $filter, $http) {
    $scope.users = [
      { id: 1, name: 'awesome user1', status: 2, group: 4, groupName: 'admin', isActive: true },
      { id: 2, name: 'awesome user2', status: undefined, group: 3, groupName: 'vip', isActive: false },
      { id: 3, name: 'awesome user3', status: 2, group: null }
    ];

    $scope.statuses = [
      { value: 1, text: 'status1' },
      { value: 2, text: 'status2' },
      { value: 3, text: 'status3' },
      { value: 4, text: 'status4' }
    ];

    $scope.groups = [];
    $scope.loadGroups = function () {
        return $scope.groups.length ? null : $http.get('/groups').success(function (data) {
            $scope.groups = data;
        });
    };

    $scope.showGroup = function (user) {
        if (user.group && $scope.groups.length) {
            var selected = $filter('filter')($scope.groups, { id: user.group });
            return selected.length ? selected[0].text : 'Not set';
        } else {
            return user.groupName || 'Not set';
        }
    };

    $scope.showStatus = function (user) {
        var selected = [];
        if (user.status) {
            selected = $filter('filter')($scope.statuses, { value: user.status });
        }
        return selected.length ? selected[0].text : 'Not set';
    };

    $scope.checkName = function (data, id) {
        //if (id === 2 && data !== 'awesome') {
        //    return "Username 2 should be `awesome`";
        //}
    };

    $scope.saveUser = function (data, id) {
        //$scope.user not updated yet
        angular.extend(data, { id: id });
        return $http.post('/saveUser', data);
    };

    // remove user
    $scope.removeUser = function (index) {
        $scope.users.splice(index, 1);
    };

    // add user
    $scope.addUser = function () {
        $scope.inserted = {
            id: $scope.users.length + 1,
            name: '',
            status: null,
            group: null
        };
        $scope.users.push($scope.inserted);
    };
});

